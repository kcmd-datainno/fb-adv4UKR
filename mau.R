#!/usr/bin/env Rscript

# clear all variables
rm(list = ls())

library(data.table)
library(ggplot2)
library(eurostat)
library(giscoR)
library(countrycode)
library(ggrepel)
require(scales)
library(ggpubr)

source("unhcr_data.R")

data_dir <- "data"
output_dir <- "output"

estat_nuts0 <-
  gisco_get_countries(
    resolution = "60",
    epsg = "3035",
    cache = TRUE,
    update_cache = TRUE,
    country = setdiff(eu_countries$code, "UK") # don't include UK
  )

# ukr citizens nuts0
# we are interested in ukr citizens under 18 yo, the same we ask for with FB API
ukr_pop_nuts0 <-
  get_eurostat(
    id = "migr_pop1ctz",
    time_format = "num",
    filters = list(
      geo = setdiff(eu_countries$code, "UK"),
      sex = "T",
      citizen = "UA",
      unit = "NR",
      age = c("TOTAL", "Y_LT15", "Y15-19"),
      time = 2019:2021
    )
  )
setDT(ukr_pop_nuts0)

# combine 2021, 2020, and 2019 to have as much data as possible and a unique list of countries
#https://stackoverflow.com/a/29497254/1979665
ukr_pop_nuts0 <-
  ukr_pop_nuts0[, .SD[which.max(time)], by = list(geo, age)]

# remove countries with no age info
remove_countries <- ukr_pop_nuts0[is.na(values), .(values = sum(values)), by = list(geo)][, geo, ]
ukr_pop_nuts0 <- ukr_pop_nuts0[!geo %in% remove_countries, , ]

# get population under 18
ukr_pop_nuts0[age != "TOTAL", age := "Y_LT19", ]
ukr_pop_nuts0 <- ukr_pop_nuts0[, .(values = sum(values)), by = list(geo, age)]
ukr_pop_ge18_eu <-
  ukr_pop_nuts0[, .(values = abs(first(values)-last(values))), by = list(geo)]

# MAU from Fb API
dfFBWBGeo <- fread(file.path(data_dir, "dfFBWBGeo.csv"))
# POSIXct to Date
dfFBWBGeo[, timestampMean := as.Date(timestampMean), ]
# order by timestamp
dfFBWBGeo <- dfFBWBGeo[order(timestampMean), , ]

# add iso country codes
dfFBWBGeo[, iso2 := countrycode(loc_name, 'country.name', 'iso2c'), ]
dfFBWBGeo[, iso3 := countrycode(loc_name, 'country.name', 'iso3c'), ]
ukr_pop_ge18_eu[, iso2 := geo, ]
ukr_pop_ge18_eu[, iso3 := countrycode(geo, 'iso2c', 'iso3c'), ]

# correct some codes
dfFBWBGeo[iso2 == 'GR', iso2 := 'EL', ]
ukr_pop_ge18_eu[iso2 == 'GR', iso2 := 'EL', ]

# apply needed filters (UKR speaking users, 'everyone in this location', only EU27, etc.)
ukr_mau <-
  dfFBWBGeo[flex_spec == 'Ukrainian' &
              loc_type == 'everyone in this location' &
              iso2 %in% setdiff(eu_countries$code, "UK"), , ]

# stop at 5th week since the start of the war
startDate <- unique(dfFBWBGeo[flex_spec == 'Ukrainian', timestampMean, ])[1]
endDate <- unique(dfFBWBGeo[flex_spec == 'Ukrainian', timestampMean, ])[6]
ukr_mau <- ukr_mau[timestampMean <= endDate, , ]

# calculate FB penetration rate in Ukraine using prewar MAU
ukr_fb_users <-
  dfFBWBGeo[flex_spec == 'Ukrainian' &
              loc_type == 'everyone in this location' &
              iso2 == 'UA' & timestampMean == startDate, mau, ]

# get ukr pop over 18 yo
ukr_pop_ge18_ua <-
  get_eurostat(
    id = "migr_pop1ctz",
    time_format = "num",
    filters = list(
      geo = "UA",
      sex = "T",
      citizen = "TOTAL", # filter on UA citizens returns NA
      unit = "NR",
      age = c("TOTAL", "Y_LT15", "Y15-19"),
      time = 2021
    )
  )
setDT(ukr_pop_ge18_ua)
# remove population below 18 yo
ukr_pop_ge18_ua <-
  ukr_pop_ge18_ua[age == "TOTAL", values, ] -
  ukr_pop_ge18_ua[age %in% c("Y_LT15", "Y15-19"), sum(values), ]

# calcualte FB penetration rate in UKR for users above 18 yo
fb_pr <-  ukr_fb_users/ukr_pop_ge18_ua

# adjust mau estimates with penetration rate
ukr_mau[, mau_adj := mau/fb_pr, ]

## MAU vs STOCKS ##
ukr_mau_prewar <- ukr_mau[timestampMean == min(timestampMean), , ]
merged <-
  ukr_mau_prewar[ukr_pop_ge18_eu[, .(iso2, values), ],
                 on = "iso2",
                 nomatch = 0,
                 allow.cartesian = TRUE]

# print correlation coefficient (calculated with Pearson)
print(cor.test(merged[, log(values), ], merged[, log(mau), ]))
print(cor.test(merged[, log(values), ], merged[, log(mau_adj), ]))

# wide to long format for plotting facets
mau_vars <- c("mau", "mau_adj")
plot_vars <- c("iso2", "values", mau_vars)
merged_long <-
  melt(merged[, ..plot_vars],
       measure.vars = mau_vars,
       variable.name = "mau_type", value.name = "mau_val")

# custom theme
my_thm <-
  list(
    theme(
      legend.title = element_text(size = 10),
      plot.title = element_text(size = 10),
      axis.title = element_text(size = 10),
      axis.text = element_text(size = 10),
      legend.text = element_text(size = 10)
    )
  )

p <-
  ggplot(merged_long,
         aes(
           x = values,
           y = mau_val,
           label = iso2
         )) +
  geom_point(size = 0.3) +
  geom_abline(slope = 1, intercept = 0, color = "blue") +
  # geom_smooth(size = 0.3,
  #             method = lm,
  #             se = FALSE) +
  stat_cor(method = "pearson",
           r.accuracy = 0.01,
           aes(label = ..r.label..)) +
  geom_text_repel(size = 3,
                  max.overlaps = Inf) +
  scale_x_continuous(labels = comma, trans='log10') +
  scale_y_continuous(labels = comma, trans='log10') +
  facet_wrap(~mau_type, scales = "free_y", ncol = 1) +
  xlab("Diaspora \u2265 18") +
  ylab("MAU") +
  my_thm

p

ggsave(
  file.path(output_dir,
            paste0("maucorr2.jpg")),
  width = 85,
  height = 85,
  units = "mm",
  dpi = 300,
  last_plot(),
  bg = "white"
)

## UKR STOCK PERC INCREASE BY COUNTRY ##

# use UKR stock from Francesco since they include all EU27
ukr_diaspora <- fread(file.path(data_dir, "UKR_diaspora.csv"))

# calculate relative MAU change by date
ukr_mau[, mau_diff := mau - first(mau), by = list(iso3)]
ukr_mau[, mau_adj_diff := mau_adj - first(mau_adj), by = list(iso3)]

# calculate perc increase of MAU relative to first week by country
ukr_mau[, mau_change_perc := mau_diff / first(mau), by = list(iso3)]

# merge with UKR population stocks
merged2 <-
  ukr_mau[ukr_diaspora[, list(iso3 = `ISO3 Country Code`,
                              ukr_pop = Stock), ],
          on = "iso3",
          nomatch = 0,
          allow.cartesian = TRUE]

# estimated stock change
merged2[,
        ukr_pop_diff := ukr_pop * mau_change_perc,
        by = list(iso2)]

merged2_last <-
  merged2[timestampMean == max(timestampMean), , ]

merged2_last[, ukr_pop_diff_perc := ukr_pop_diff/sum(ukr_pop_diff)*100, ]

# only keep high relative maus (for better plotting)
perc_threshold <- 2

p <-
  ggplot(data = merged2_last[ukr_pop_diff_perc > perc_threshold, , ],
         aes(x = reorder(iso2, ukr_pop_diff_perc),
             y = ukr_pop_diff_perc)) +
  geom_col() +
  scale_y_continuous(
    limits = c(0, 40),
    labels = function(x)
      paste0(x, "%")
  ) +
  coord_flip() +
  geom_text(aes(label = paste0(round(ukr_pop_diff_perc, 0), "%"),
                y = ukr_pop_diff_perc + 4)) +
  xlab(NULL) +
  ylab(NULL)
p + my_thm

ggsave(
  file.path(output_dir,
            paste0("mau_incr_perc2.jpg")),
  width = 84,
  height = 70,
  units = "mm",
  dpi = 600,
  last_plot(),
  bg = "white"
)

## MAU TREND VS UNHCR

# get data from UNHCR
unhcr_geoid <- fread(file.path(data_dir, "unhcr", "EU27_geoid.csv"))

# Refugees from Ukraine recorded by country (UNHCR)
#https://data2.unhcr.org/en/situations/ukraine
unhcr_arr_ts <-
  unhcr_geoid[,
              do.call(rbind.data.frame,
                      get_unhcr_ts(id)$timeseries),
              by = list(name)]

# set date column format
unhcr_arr_ts[, data_date :=  as.Date(data_date, format =  "%Y-%m-%d"), ]
# add iso2 and iso3
unhcr_arr_ts[, iso2 := countrycode(name, 'country.name', 'iso2c'), ]
unhcr_arr_ts[, iso3 := countrycode(name, 'country.name', 'iso3c'), ]

# keep only dates available in MAU dataset
unhcr_arr_ts <- unhcr_arr_ts[data_date >= startDate & data_date <= endDate, , ]

# keep only dates available in MAU dataset
unhcr_arr_ts <-
  unhcr_arr_ts[data_date <= ukr_mau[, max(timestampMean), ], , ]

# calculate relative arrivals change by date
unhcr_arr_ts[, cum_arrivals_diff := individuals - first(individuals), by = list(iso2)]

# normalize absolute changes using absolute max found in countries
ukr_mau[, mau_diff_norm := mau_diff / max(mau_adj_diff), ]
ukr_mau[, mau_adj_diff_norm := mau_adj_diff / max(mau_adj_diff), ]
unhcr_arr_ts[, cum_arrivals_diff_norm := cum_arrivals_diff / max(cum_arrivals_diff), ]

# this is just for plotting label shape
unhcr_arr_ts[, label := "UNHCR data", ]

# change in all weeks (consider same countries as in previous plot)
included_countries <- merged2_last[ukr_pop_diff_perc > perc_threshold, iso2, ]
included_countries <- unique(c(unhcr_arr_ts[, iso2, ], included_countries))

# custom theme
my_thm <-
  list(
    theme(
      legend.title = element_text(size = 10),
      plot.title = element_text(size = 10),
      axis.title = element_text(size = 10),
      axis.text = element_text(size = 10),
      legend.text = element_text(size = 10),
      strip.text = element_text(size=10,lineheight=2),
    )
  )
p <-
  ggplot() +
  geom_ribbon(
    data = ukr_mau[iso2 %in% included_countries, , ],
    aes(
      x = timestampMean,
      y = mau_diff_norm,
      ymin = mau_diff_norm,
      ymax = mau_adj_diff_norm,
      color = iso2,
      fill = iso2),
    alpha = 0.3,
    linetype = "longdash") +
  # data from UNHCR
  geom_line(
    data = unhcr_arr_ts[data_date <= ukr_mau[, max(timestampMean), ] &
                          iso2 %in% included_countries, , ],
    aes(
      y = cum_arrivals_diff_norm,
      x = data_date,
      color = iso2),
    linetype = "dashed"
    ) +
  geom_point(
    data = unhcr_arr_ts[data_date <= ukr_mau[, max(timestampMean), ] &
                          iso2 %in% included_countries, , ],
    aes(
      y = cum_arrivals_diff_norm,
      x = data_date,
      fill = iso2,
      shape = label,
      colour = NULL),
    size = 0.7
  ) +
  facet_wrap(~ iso2, nrow = 4) +
  scale_x_date(breaks = ukr_mau[iso2 %in% included_countries, timestampMean, ],
               labels = ukr_mau[iso2 %in% included_countries, format(timestampMean, "%d%b%y"), ]) +
  scale_y_continuous(labels = comma, trans='log10') +
  scale_shape_manual(values = c(21)) +
  xlab(NULL) +
  ylab("MAU absolute change") +
  scale_fill_discrete(guide="none") +
  scale_color_discrete(guide="none") +
  labs(shape = "") +
  theme(
    legend.position="bottom",
    legend.justification = "left",
    legend.spacing.x = unit(0.05, 'cm'),
    legend.margin = margin(0,-0.01,0,0, unit="cm")) +
  theme(axis.text.x = element_text(angle = 55, hjust = 1))
p + my_thm

ggsave(
  file.path(output_dir,
            paste0("mau_diff_norm2.jpg")),
  width = 100,
  height = 100,
  units = "mm",
  dpi = 300,
  last_plot(),
  bg = "white"
)
